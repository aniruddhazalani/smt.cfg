
import csv,itertools,getopt
import sys,random,subprocess

def wordList(filename):
    a = []

    for i in open(filename, 'r'):
        a.append(i.strip())

    return a

def csvRead(filename):
    a = []

    with open(filename) as csvfile:
        reader = csv.reader(csvfile,delimiter=',')
        for row in reader:
            a.append(row)

    return a

def makeDict(rules):
    dictionary = dict()

    for production in rules:
        if production[0] in dictionary:
            dictionary[production[0]].append(production[1:])
        else:
            dictionary[production[0]] = [production[1:]]

    return dictionary

def extractGrammarFromCSV(f):
    dic,idx = dict(),0
    term,nterm,token = set(),set(),set()
    dic = makeDict(csvRead(f))
    for lhs in dic:
        token.add(lhs)
        nterm.add(lhs)
        for rhs in dic[lhs]:
            for idx,tok in enumerate(rhs):
                token.add(tok)
    
    term = token.difference(nterm)
    return (dic,term,nterm)

def extractGrammar(f):
    dic,idx = dict(),0
    startSym = ''
    term,nterm,token = set(),set(),set()

    for line in open(f):
        a = line.split(' ')[:-1]
        for index in range(0,len(a)):
            if len(a[index]) == 3:
                a[index] = a[index][1]
        if a[0] == '':
            start = prevStart
            dic[start].append(a[3:])
        else:
            if idx == 0:
                startSym = a[0]
                idx += 1
            start = a[0]
            prevStart = start
            dic[start] = [a[2:]]


    for lhs in dic:
        token.add(lhs)
        nterm.add(lhs)
        for rhs in dic[lhs]:
            for idx,tok in enumerate(rhs):
                token.add(tok)
    
    term = token.difference(nterm)
    return (startSym,dic,term,nterm)

