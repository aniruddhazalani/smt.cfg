import csv,itertools,getopt
import sys,random,subprocess

def csvRead(filename):
    a = []

    with open(filename) as csvfile:
        reader = csv.reader(csvfile,delimiter=',')
        for row in reader:
            a.append(row)

    return a

if __name__ == '__main__':

    opts, args = getopt.getopt(sys.argv[1:], "hi:")    
    inputFile = ''

    for opt, arg in opts:
        if opt == '-h':
            print "usage python extractTokens.py -i <input_file>"

        elif opt == '-i':
            inputFile = arg

    rules = csvRead(inputFile)
    nonTerm = [rule[0] for rule in rules]
    nonTerm = set(nonTerm)
    for i in nonTerm:
        print i
    print '\n'
    term = []

    for rule in rules:
        for token in rule:
            if token not in nonTerm:
                term.append(token)

    term = set(term)

    for i in term:
        print i
