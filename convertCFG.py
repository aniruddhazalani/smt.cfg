import sys
import csv,itertools,getopt
sys.path.insert(0,'../z3/build/')

from z3 import *
from helper import *

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate


@static_vars(counter=0)
def count():
    count.counter += 1
    return count.counter

def getNonterm():
    return 'A' + str(count())

def makeDictStr(rules):
    dictionary = dict()

    for production in rules:
        if production[0] in dictionary:
            dictionary[production[0]].append(production[1:][0])
        else:
            dictionary[production[0]] = production[1:]

    return dictionary


def makeDictCFG(rules):
    dictionary = dict()

    for production in rules:
        if production[0] in dictionary:
            dictionary[production[0]].append(production[1:])
        else:
            dictionary[production[0]] = [production[1:]]

    return dictionary

def convert(prod, start, terminals, lhs):
    dic = dict()
    end = start + 1

    if len(prod) <= end:
        dic[lhs] = prod[start:end]
        return [[lhs, prod[start:end]]]

    while prod[end] not in terminals:
        end += 1
        if len(prod) <= end:
            break

    if len(prod) <= end:
        dic[lhs] = prod[start:end]
        return [[lhs,prod[start:end]]]
    else:
        newNonTerm = getNonterm()
        a = prod[start:end]
        a.append(newNonTerm)
        dic[lhs] = a
        ans = [[lhs,a]]
        for i in convert(prod,end,terminals,newNonTerm):
            ans.append(i)

        return ans

def modifyCFG(rules, terminals):
    ans = []
    for lhs in rules:
        for prod in rules[lhs]:
            #print lhs,prod
            for i in convert(prod,0,terminals, lhs):
                ans.append(i)
            #print "\n"
    return makeDictStr(ans)

def numNontermGen(prod, nonterminals):
    count = 0

    for token in prod:
        if token in nonterminals:
            count += 1
    return count

def numTermGen(prod,nonterminals):
    return len(prod) - numNontermGen(prod,nonterminals)

def encodeAbstraction(startsym, tvarlist, posVarList, strVarList, querystringencoding):

    k,querycond = [],[]
    k.append(tvarlist[0])
    k.append(tvarlist[1])

    k = list(itertools.chain(*k))
    k.append(0)
    k.append(0)#sum(querystringencoding))
    k.append(1)
    k.append(strVarList[0][1])
    querycond.append(startsym(k))

    for i in range(0,len(tvarlist[0])):
        querycond.append(tvarlist[0][i] == 0)
    for i in range(0,len(tvarlist[1])):
        querycond.append(tvarlist[1][i] == querystringencoding[i])
    #querycond.append(tvarlist[1][5] > 0)

    return querycond

def inclusionExclusionStrings(startsym, tvarlist, posVarList, strVarList, termIndex):
    k,querycond = [],[]
    k.append(tvarlist[0])
    k.append(tvarlist[1])
    

    k = list(itertools.chain(*k))
    k.append(0)
    k.append(0)#sum(querystringencoding))
    k.append(1)
    k.append(strVarList[0][1])
    querycond.append(startsym(k))

    for i in range(0,len(tvarlist[0])):
        querycond.append(tvarlist[0][i] == 0)

    for i in range(0,len(tvarlist[1])):
        querycond.append(tvarlist[1][i] >= 0)

    querycond.append(tvarlist[1][termIndex] > 0)

    return querycond


def encodeFuncGen(terminals, nonterminals, prod, funDict, tvarList, posVarList, strVarList):

    termList = list(terminals)  #this assumes that the order is preserved while converting a set to a list

    cntTerm = [0 for i in range(0,len(terminals))]

    lhs,rhs=[],[]

    if numNontermGen(prod,nonterminals) == 0:
        "the number of tokens could be 1 or 0"
        assert(len(prod)<=1)

        for token in prod:
            assert(len(prod)==1)
            #add a special condition here specifying tokens to generate a query involving specific tokens
            cntTerm[termList.index(token)] += 1 # TODO paramaterize here
            rhs.append(strVarList[0][1] == strVarList[0][0]*100 + termList.index(token) + 1)

        for i in range(0,len(terminals)):
            rhs.append(tvarList[1][i] == tvarList[0][i] + cntTerm[i])

        assert(numNontermGen(prod,nonterminals) <= 1)
        rhs.append(posVarList[0][1] == posVarList[0][0] )#+ numTermGen(prod,nonterminals))

        lhs.append(tvarList[0])
        lhs.append(tvarList[1])
        lhs = list(itertools.chain(*lhs))
        lhs.append(posVarList[0][0])
        lhs.append(posVarList[0][1])

        if numTermGen(prod, nonterminals) == 0: #empty production
            rhs.append(strVarList[0][0] == strVarList[0][1])
            lhs.append(strVarList[0][0])
            lhs.append(strVarList[0][1])
        else:
            lhs.append(strVarList[0][0])
            lhs.append(strVarList[0][1])
#            lhs.append(termList.index(prod[0]))

    else:
        curr = 0
        indexPrevToken = 0
        prevToken = ''
        for idx,token in enumerate(prod):
            if token in nonterminals:
                t = []
                for idx,var in enumerate(tvarList[curr]):
                    t.append(var + cntTerm[idx])

                assert(sum(cntTerm) <= 1)

                k = []
                k.append(t)
                k.append(tvarList[curr+1])
                k = list(itertools.chain(*k))

                k.append(posVarList[curr][0] )#+ sum(cntTerm))

                k.append(posVarList[curr][1])

                if indexPrevToken == 0:
                    #print "no prev token",prevToken,prod, token
                    k.append(strVarList[curr][0])

                else:
                    #print "prev token",prevToken,prod, token
                    k.append(strVarList[curr][0]*100 + indexPrevToken )

                k.append(strVarList[curr][1])

                indexPrevToken = 0
                prevToken = ''

                #equate current start and the previous end
                if curr > 0:
                    rhs.append(posVarList[curr][0] == posVarList[curr-1][1])
                    rhs.append(strVarList[curr][0] == strVarList[curr-1][1])

                rhs.append(funDict[token](k))
                curr += 1
                cntTerm = [0 for i in range(0,len(terminals))]

            else:
                cntTerm[termList.index(token)] += 1 # TODO paramaterize here
                assert(sum(cntTerm) == 1)
                assert(idx != len(prod)-1)
                indexPrevToken = termList.index(token) + 1
                prevToken = token

                if idx == len(prod) - 1:
                    "code should never reach here"
                    print "invalid location reached grammar modified wrongly"
                    exit(-1)
                    for i in range(0,len(terminals)):
                        rhs.append(tvarList[curr+1][i] == tvarList[curr+1][i] + cntTerm[i]) #check on this step the use of set

                    curr += 1

        lhs.append(tvarList[0])
        lhs.append(tvarList[curr])
        lhs = list(itertools.chain(*lhs))
        lhs.append(posVarList[0][0])
        lhs.append(posVarList[curr-1][1])
        lhs.append(strVarList[0][0])
        lhs.append(strVarList[curr-1][1])

    return (lhs,rhs)

def generateString(stringAbs, rules, terminals, nonterminals, termDiff={}, termC={}):
    modCFGDict = modifyCFG(rules, terminals)

    termList,ntermList = list(terminals),list(nonterminals)
    #if things break then increase the number according the max length of a rule
    tempVariable = [chr(ord('a')+i) for i in range(0,26)]

    tvarList,posVarList,strVarList = [],[],[]
    funList,funDict = [],{}
    I,B = IntSort(),BoolSort()

    """To encode functions set up correct number of arguments
        extra three arguments to incorporate start,end and the index of the terminal
    """

    fpStringGen = Fixedpoint()

    # 4 variables are added corresponding to positionvar and stringvar
    arg = [I for i in range(0,2*len(termList) + 4)]
    arg.append(B)

    for token in nonterminals:
        funList.append(Function(token,arg))
        funDict[token] = funList[-1]


    for i in range(1,count.counter+1):
        token = 'A' + str(i)
        funList.append(Function(token,arg))
        funDict[token] = funList[-1]
        nonterminals.add(token) #update nonterminals list

    """register_relation statement"""
    for nonTerm in funList:
        fpStringGen.register_relation(nonTerm)

    """declare the temporary variables here according to the count of terminals"""
    for var in tempVariable:
        tvar,posVar,strVar = [],[],[]

        for i in range(0,len(terminals)):
            tvar.append(Ints(var+str(i))[0])

        fpStringGen.declare_var(tvar)
        tvarList.append(tvar)

        for i in range(0,2):
            posVar.append(Ints(var+str(i)+str(i))[0])

        fpStringGen.declare_var(posVar)
        posVarList.append(posVar)

        for i in range(0,2):
            strVar.append(Ints(var+str(i)+str(i)+str(i))[0])

        fpStringGen.declare_var(strVar)
        strVarList.append(strVar)


    startSym = funDict['S']

    for idx,fun in enumerate(funList):
        for prod in  modCFGDict[fun.name()]:
            (lhs,rhs) = encodeFuncGen(terminals, nonterminals, prod, funDict, tvarList, posVarList, strVarList)
            fpStringGen.rule(fun(lhs),rhs)

    fpStringGen.set('fixedpoint.generate_proof_trace',True)
    
    for tok in termDiff:
        query = inclusionExclusionStrings(startSym,tvarList,posVarList,strVarList,list(termC).index(tok))
        r = fpStringGen.query(And(query))
        print r,"here"
        if r == sat:
            num =  fpStringGen.get_answer().num_args()
            string = fpStringGen.get_answer().arg(num-1).arg(len(tvarList[0]))
            print string
            st = ''
            string  = str(string)
            a,idx = 0,int(string[0])
            while a < len(string) :
                if a > 0:
                    st = st + ' '+ termList[idx-1]
                if a >= len(string) -1:
                    break
                idx = int(string[a+1])*10 + int(string[a+2])
                a+=2
            print st

    for ceAbs in stringAbs:
        query = encodeAbstraction(startSym, tvarList, posVarList, strVarList, ceAbs)
        r = fpStringGen.query(And(query))
        print r,fpStringGen.get_answer().num_args()
        if r == sat:
            num =  fpStringGen.get_answer().num_args()
            string = fpStringGen.get_answer().arg(num-1).arg(len(ceAbs))
            st = ''
            string  = str(string)
            a,idx = 0,int(string[0])
            while a < len(string) :
                if a > 0:
                    st = st + ' '+ termList[idx-1]
                if a >= len(string) -1:
                    break
                idx = int(string[a+1])*10 + int(string[a+2])
                a+=2
            print st

if __name__ == '__main__':
    opts, args = getopt.getopt(sys.argv[1:], "dhi:")
    inputdir = ''
    for opt, arg in opts:
        if opt == '-h':
            print "usage python parseParserDebugFile.py -i <input_directory>"

        elif opt == '-i':
            inputdir = arg

    #nonTermCor = set(wordList(inputdir+'/n-correct'))
    #correctRules = csvRead(inputdir+'/correct-grammar')
    #correctDict = makeDictCFG(correctRules)
    correctDict,termC,nonTermCor = extractGrammarFromCSV(inputdir+'/correct-grammar')
    buggyDict,termB,nonTermBug = extractGrammarFromCSV(inputdir+'/r')
    terminals = termB.union(termC)
    counterExampleAbs = csvRead(inputdir+'/abs1')
    a = []
    for ce in counterExampleAbs:
        a.append(map(lambda x: int(x), ce))

    #generateString(a, correctDict, terminals, nonTermCor, termC.difference(termB), termC)
    generateString(a, correctDict, terminals, nonTermCor)
