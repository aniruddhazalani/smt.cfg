(declare-var x Int)
(declare-var y Int)

(declare-rel I (Int) interval_relation)
(declare-rel I1 (Int) interval_relation)
(declare-rel err (Int) interval_relation)

(rule (=> (= x 0) (I x) ))
(rule (=> (and (= y (+ x 1)) (I x) ) (I1 y) ))
(rule (=> (and (> y 2) (I1 y)) (err y) ))

(query (err y)
    :engine pdr
    :use-farkas true
    :print-answer true
    )
;(get-answ)
