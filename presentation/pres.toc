\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Motivation}{5}{0}{1}
\beamer@sectionintoc {2}{Problem Statement}{13}{0}{2}
\beamer@sectionintoc {3}{Algorithm}{22}{0}{3}
\beamer@subsectionintoc {3}{1}{Horn Clauses}{23}{0}{3}
\beamer@subsectionintoc {3}{2}{Horn Clause Solvers}{24}{0}{3}
\beamer@sectionintoc {4}{Grammars as Horn Clauses}{25}{0}{4}
\beamer@subsectionintoc {4}{1}{Abstraction Function}{27}{0}{4}
\beamer@subsectionintoc {4}{2}{Grammars as Horn Clauses}{31}{0}{4}
\beamer@sectionintoc {5}{Certificate Generation}{33}{0}{5}
\beamer@subsectionintoc {5}{1}{Certificate Generation}{34}{0}{5}
\beamer@sectionintoc {6}{Abstraction Computation}{36}{0}{6}
\beamer@sectionintoc {7}{Counterexample Generation}{38}{0}{7}
