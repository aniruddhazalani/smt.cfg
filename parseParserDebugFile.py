 #!/usr/bin/env python3
import csv,itertools,getopt,copy
import sys,random,subprocess
sys.path.insert(0,'../z3/build/')
from z3  import *
from ast  import *
from convertCFG import *
from helper import *

#set_option(trace = True)
set_option(auto_config = False)
set_option(unsat_core = True)
#enable_trace("pdr")

debug = False

def numNonterm(prod, nonterminals):
    count = 0

    for token in prod:
        if token in nonterminals:
            count += 1
    return count

def solveSmt(startSym, correctGrammarEncode, query, tvarList, maxLen, flag):
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(tvarList[0])):
        queryCond.append(tvarList[0][i] == 0)
        queryCond.append(tvarList[1][i] > 0)

#   length constraint
    queryCond.append(sum(tvarList[1]) <= maxLen)

#    smtSolver = Then('simplify', 'smt').solver()
#    smtSolver = Tactic('qfnra-nlsat').solver()
 #   smtSolver =  Then('simplify', 'nla2bv', 'smt').solver()

    smtSolver = Solver()
#    smtSolver.set(unsat_core=True)
    smtSolver.set(mbqi=True)
    smtSolver.set(pull_nested_quantifiers=True)

#    smtSolver = Then('simplify', 'elim-term-ite', 'solve-eqs', 'smt').solver()
    smtSolver.assert_and_track(And(correctGrammarEncode),'correctGrammarEncode')
    #print smtSolver
    smtSolver.assert_and_track(And(queryCond),'model')
#    print correctGrammarEncode
    for idx,certificate in enumerate(query):
        if flag:
            if idx == len(query) - 1:
                smtSolver.assert_and_track(certificate, 'condition'+str(idx))

            else:
                smtSolver.assert_and_track(certificate, 'condition'+str(idx))

        else:
            smtSolver.assert_and_track(certificate, 'condition'+str(idx))

    a =  smtSolver.to_smt2()
    """
    This part has been written to write a smt-lib2 format file and run it if the python api are buggy
    for i in tvarList[1]:
        a += '(get-value (' + str(i) + '))\n'
#    a += '(get-model)'
    f = open('a.smt', 'w')
    f.write(a)
    f.close()
    os.system('../z3/build/z3 -smt2  a.smt > out')

    val = []
    for i in open('out','r'):
        a = i.split(' ')
        if a[0][:2] == '((' :
            val.append(int(a[1][:-3]))

    return val
"""
    if str(smtSolver.check()) == 'unsat':
        #print "unsat_core"
        #print smtSolver.unsat_core()
        return (unsat, False)

    return (sat, smtSolver.model())


def computeOnToken(token):
    return 1

def emitStrings(startSym, fpOracle, certiList, funList, tvarList, maxLen):
    output = []
    for i in range(0,len(certiList)):
        certiList[i][0] = certiList[i][0]
        outStringEncoding  = findString(startSym, fpOracle, certiList, funList, tvarList, maxLen, i+1)
        if outStringEncoding[0] == sat:
            a = [i for i in outStringEncoding]
            output.append(a[1])
            #print a[1]

    return output


def generateLoopQuery(startSym, tvarList, queryStringEncoding):

    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(tvarList[0])):
        queryCond.append(tvarList[0][i] == 0)

    for i in range(0,len(tvarList[1])):
        queryCond.append(tvarList[1][i] == queryStringEncoding[i])

    return queryCond

def generateQueryIncExc(startSym, tvarList,  index):

    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(tvarList[0])):
        queryCond.append(tvarList[0][i] == 0)

    queryCond.append(tvarList[1][index] > 0)

    return queryCond

def generateQuery(startSym, tvarList, queryString, termList):

    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(termList)):
        queryCond.append(tvarList[0][i] == 0)

    cntTerm = [0 for i in range(0,len(termList))]
    print termList
    for token in queryString:
        #pass
        cntTerm[termList.index(token)] += computeOnToken(token) # TODO paramaterize here

    for i in range(0,len(termList)):
        queryCond.append(tvarList[1][i] == cntTerm[i])#random.randint(0,8))# +  random.randint(0,8))

    return queryCond

'''
    use a for input variables and b for output variables
    for each terminal there are two variables
    compute index of each terminal
'''

def encodeFunc(terminals, nonterminals, prod, funDict, tvarList):

    termList = list(terminals)  #this assumes that the order is preserved while converting a set to a list

    cntTerm = [0 for i in range(0,len(terminals))]

    lhs,rhs=[],[]

    if numNonterm(prod,nonterminals) == 0:

        for token in prod:
            #add a special condition here specifying tokens to generate a query involving specific tokens
            cntTerm[termList.index(token)] += computeOnToken(token) # TODO paramaterize here

        for i in range(0,len(terminals)):
            rhs.append(tvarList[1][i] == tvarList[0][i] + cntTerm[i])

        lhs.append(tvarList[0])
        lhs.append(tvarList[1])
        lhs = list(itertools.chain(*lhs))

    else:
        curr = 0

        for idx,token in enumerate(prod):
            if token in nonterminals:
                t = []

                for idx,var in enumerate(tvarList[curr]):
                    t.append(var + cntTerm[idx])

                k = []
                k.append(t)
                k.append(tvarList[curr+1])
                rhs.append(funDict[token](list(itertools.chain(*k))))
                curr += 1
                cntTerm = [0 for i in range(0,len(terminals))]

            else:
                cntTerm[termList.index(token)] += computeOnToken(token) # TODO paramaterize here

                if idx == len(prod) - 1:
                    for i in range(0,len(terminals)):
                        rhs.append(tvarList[curr+1][i] == tvarList[curr][i] + cntTerm[i]) #check on this step the use of set

                    curr += 1

        lhs.append(tvarList[0])
        lhs.append(tvarList[curr])
        lhs = list(itertools.chain(*lhs))

    return (lhs,rhs)


# TODO add debug feature

def findString(startSym, fpOracle, queryList, funList ,tvarList, maxLen, flag):
    #print queryList
    fpOracle.set('fixedpoint.engine','pdr')
    fpOracle.set('fixedpoint.generate_proof_trace',True)
    #fpOracle.set('fixedpoint.pdr.bfs_model_search',False)
    #fpOracle.set('fixedpoint.pdr.flexible_trace',True)
    #print main_ctx() == query.arg(1).ctx

    k = []
    k.append(tvarList[0])
    k.append(tvarList[1])
    k = list(itertools.chain(*k))

    fpOracle.declare_var(k)
    """ this piece of code is useful when query is encoded as a rule
    I,B = IntSort(),BoolSort()

    arg = [I for i in range(0,2*len(tvarList[0]))]
    arg.append(B)
    queryFunc = Function('queryFunc',arg)
    fpOracle.register_relation(queryFunc)
    lhs = queryFunc(k)
    """

    rhs = []
    rhs.append(startSym(k))

    for i in range(0,len(tvarList[0])):
        rhs.append(tvarList[0][i] == 0)
        rhs.append(tvarList[1][i] >= 0)
    #length constraint
    #rhs.append(sum(tvarList[1])<= maxLen*2)
    for cond in equateVar(queryList[0][1], tvarList[0], tvarList[1]):
        rhs.append(cond)

    if flag < 0:
        print "here"
        if len(queryList) == 1:
            print len(queryList[0])
            for idx1 in range(0,len(queryList[0][0])):
                rhs1= []
                c = ''
                for idx2,cond in enumerate(queryList[0][0]):
                    if idx1 == idx2:
                        c = cond
                        rhs1.append(Not(And(cond)))
                    else:
                        rhs1.append(And(cond))
                rhs.append(Not(And(rhs1)))
                print c.arg(1)
                result = fpOracle.query(And(rhs))
                rhs.pop()
                print result
                if result == sat:
                    print fpOracle.get_answer().arg(2)


    if flag == 0:
        for idx,query in enumerate(queryList):
            t = [i for i in query[0]]
            rhs.append(And(t))
            """for i in t:
                rhs.append(Not(And(i)))
                result = fpOracle.query(And(rhs))
                a = [fpOracle.get_answer().arg(2).arg(i+len(tvarList[0])) for i in range(0,len(tvarList[0])) ]
                print i
            """
    else:
        for idx,query in enumerate(queryList):
            t = [i for i in query[0]]
            if idx == flag:
                rhs.append(And(t))
            else:
                rhs.append(Not(And(t)))

    result = fpOracle.query(And(rhs))

    #print fpOracle.get_answer().arg(2), fpOracle.get_answer().arg(2).num_args(), len(tvarList[0])
    if result == sat:
        num = fpOracle.get_answer().num_args()
        if str(fpOracle.get_answer().arg(num-1).decl())[:5] != "query":
            n = fpOracle.get_answer().arg(num-1).num_args()
            a = [fpOracle.get_answer().arg(num-1).arg(n-1).arg(i+len(tvarList[0])) for i in range(0,len(tvarList[0]))]
        else:
            a = [fpOracle.get_answer().arg(num-1).arg(i+len(tvarList[0])) for i in range(0,len(tvarList[0]))]

        t = [queryList[0][1][i+len(a)] == a[i] for i in range(0,len(a))]

        while 0:
            flag -= 1
            p = []
            p.append(rhs)
            p.append([Not(And(t))])
            rhs = list(itertools.chain(*p))
            r = fpOracle.query(rhs)
            if result == sat:
                a = [fpOracle.get_answer().arg(2).arg(i+len(tvarList[0])) for i in range(0,len(tvarList[0]))]
                t = [queryList[0][1][i+len(a)] == a[i] for i in range(0,len(a))]
        return (sat,a)
        return (sat,extractStringAbstraction(fpOracle.get_answer(), funList))
    else:
        return (unsat,[])


def getFPSolver(grammardict, terminals, nonterminals):
    termList,ntermList = list(terminals),list(nonterminals)

    #if things break then increase the number according the max length of a rule
    tempVariable = [chr(ord('a')+i) for i in range(0,20)]

    tvarList = []
    funList,funDict = [],{}
    I,B = IntSort(),BoolSort()

    """To encode functions set up correct number of arguments"""
    arg = [I for i in range(0,2*len(termList))]
    arg.append(B)
    for token in nonterminals:
        funList.append(Function(token,arg))
        funDict[token] = funList[-1]

    """Initialize the two fixed point solver"""

    fp =  Fixedpoint()

#   register_relation statement
    for nonTerm in funList:
        fp.register_relation(nonTerm)

#   declare the temporary variables here according to the count of terminals
    for var in tempVariable:
        tvar = []

        for i in range(0,len(terminals)):
            tvar.append(Ints(var+str(i))[0])
        fp.declare_var(tvar)
        tvarList.append(tvar)

    return (fp,tvarList,funDict,funList)

def inclusionExclusionStrings(buggyDict, correctDict, termC, termB,  nonTermBug, nonTermCor):
    absentTerminals =  list(termC.difference(termB))
    terminals = termC.union(termB)
    termList = list(terminals)
    (fpOracle,tvarListOrcl,funDictOrcl,funListOrcl) = getFPSolver(correctDict,terminals,nonTermCor)
    print terminals

    for idx,fun in enumerate(funListOrcl):
        for idx2,prod in enumerate(correctDict[fun.name()]):
            (lhs,rhs) = encodeFunc(terminals, nonTermCor, prod, funDictOrcl, tvarListOrcl)
            fpOracle.rule(fun(lhs),rhs,str(idx)+str(idx2))

    startSym = funDictOrcl['S']
    queryCond = generateQueryIncExc(startSym, tvarListOrcl, termList.index(absentTerminals[1]))
    result = fpOracle.query(queryCond)
    print result,fpOracle.get_answer().sexpr()
    exit(-1)


def main(buggyDict, correctDict,  terminals, nonTermBug, nonTermCor, queryString):

    termList = list(terminals)
    (fp,tvarList,funDict,funList) = getFPSolver(buggyDict,terminals,nonTermBug)

    for idx,fun in enumerate(funList):
        for prod in  buggyDict[fun.name()]:
            (lhs,rhs) = encodeFunc(terminals, nonTermBug, prod, funDict, tvarList)
            fp.rule(fun(lhs),rhs,str(idx))

    startSym = funDict['S']
    queryCond = generateQuery(startSym, tvarList, queryString, termList)
    #@print "checking"
    result = fp.query(And(queryCond))
    if debug:
        print "query Condition",queryCond
        print "fp on G solved, result ", result

#    print fp.to_string(fp.get_rules())
    #print queryCond
    certiList = [fp.get_answer()]

    count = 0
    if result == sat:
        print "orignal string satisfiable"

    else:
        maxLen = len(queryString)
        queryList = []

        (fpOracle,tvarListOrcl,funDictOrcl,funListOrcl) = getFPSolver(correctDict,terminals,nonTermCor)
        for idx,fun in enumerate(funListOrcl):
            for idx2,prod in enumerate(correctDict[fun.name()]):
                (lhs,rhs) = encodeFunc(terminals, nonTermCor, prod, funDictOrcl, tvarListOrcl)
                fpOracle.rule(fun(lhs),rhs,str(idx)+str(idx2))

        while result == unsat  :

#           generates a new string satisfying the last certificate. This string is
#           in the correct grammar

            t = Tactic('qe')
            query = certiList[-1]
            modifyFuncDict  = dict()

            if len(nonTermBug) == 1:
                key =  query.children()[0].children()[0].decl()
                value = [t(query.children()[0].children()[1])[0], query.children()[0].children()[0].children()]
                modifyFuncDict[key] = value
            else:
                for i in range(0,query.num_args()):
                    key = query.arg(i).children()[0].children()[0].decl()
                    if str(key) == str(startSym):
                        value  = [t(query.arg(i).children()[0].children()[1])[0], query.arg(i).children()[0].children()[0].children()]
                        modifyFuncDict[key] = value

            """add rules"""
            q = []
            for idx,fun in enumerate(funListOrcl):
                if fun.name() == str(startSym):
                    q.append(modifyFuncDict[startSym][0])
                    q.append(modifyFuncDict[startSym][1])
            queryList.append(q)

            result, model = findString(startSym, fpOracle, queryList, funList, tvarList, maxLen, 0)
            if str(result) == 'unsat':
                #print "\n\nasdasd\n\n"
                break

            #checkStringEncoding(correctDict,startSym,terminals,model)

            queryStringEncoding = [i for i in model]
            print queryStringEncoding
            for i in range(0,len(queryStringEncoding)):
                break
                #print queryStringEncoding[i],termList[i]

            query = generateLoopQuery(startSym, tvarList, queryStringEncoding)
            result = fp.query(And(query))

            if result == unsat:
                temp = fp.get_answer()
                certiList.append(temp)
                count += 1
                print "----------------------",count
                if count > 6:
                    #print "\n\n\n\n------------------------\n\n"
                    break
                #print "count------------------------------inside\n"

            else:
                pass
                #print "sat inside loop"


        return emitStrings(startSym, fpOracle, queryList, funList, tvarList, maxLen)

# do not assume that the correct and the buggy grammar will have same set of terminals and non terminals
# implement a function here for each of them. Split up the implementation
if __name__ == '__main__':
    opts, args = getopt.getopt(sys.argv[1:], "dhi:")
    inputdir = ''

    for opt, arg in opts:
        if opt == '-h':
            print "usage python parseParserDebugFile.py -i <input_directory>"
            exit(-1)

        elif opt == '-d':
            debug  = True

        elif opt == '-i':
            inputdir = arg

    #terminals = set(wordList(inputdir+'/t'))

    #nonTermCor = set(wordList(inputdir+'/n-correct'))
    #nonTermBug = set(wordList(inputdir+'/n-bug'))
    #buggyRules = csvRead(inputdir+'/r')
    #correctRules = csvRead(inputdir+'/correct-grammar')

    queryStringList = csvRead(inputdir+'/strings')
    #buggyDict = makeDict(buggyRules)
    #correctDict = makeDict(correctRules)

    correctDict,termC,nonTermCor = extractGrammarFromCSV(inputdir+'/correct-grammar')
    buggyDict,termB,nonTermBug = extractGrammarFromCSV(inputdir+'/r')
    terminals = termB.union(termC)
    #print correctDict

    #inclusionExclusionStrings(buggyDict, correctDict, termC, termB,  nonTermBug, nonTermCor)
    for queryString in queryStringList:
        if debug:
            print "queryString = ", queryString
        counterExampleAbs =  main(buggyDict, correctDict, terminals, nonTermBug, nonTermCor, queryString)
        for i,token in enumerate(terminals):
            print i,token
        #generateString([1, 1, 2, 2, 2, 0, 4, 1, 1, 2, 1], correctDict, terminals, nonTermCor)
        #printCounterExample(counterExampleAbs,inputdir)
        print counterExampleAbs
        for j in counterExampleAbs:
            a = []
            for i in range(0,len(terminals)):
                a.append((j[i],list(terminals)[i]))
            print sorted(a)
        with open(inputdir+'/abs', "wb") as f:
            writer = csv.writer(f)
            writer.writerows(counterExampleAbs)
        exit(-1)
#        print main(correctDict, correctDict, terminals, nonterminals, queryString), "Aniruddha result"
