# README #

### Setup ###

* Build z3.
* Update the path of the z3's build directory in source to import it.


### Run ###

* parseParserDebugFile.py is the main file. It expects input directory as an argument. The input directory must contain these three files:
  >- strings, contains strings which do not parser over buggy grammar.
  >- r, buggy grammar in csv format
  >- correct-grammar, correct grammar in csv format
* usage: python parseParserDebugFile.py -i <input_directory>