
#Copyright (c) 2015 Microsoft Corporation
import sys
sys.path.insert(0,'../../z3/build')
from z3 import *
set_option(auto_config=True)

X = Array('A', IntSort(), IntSort())

a = Int('a')
b = Int('b')
c  = Int('c')
d  = Int('d')

e  = Int('e')
f  = Int('f')
x  = Int('x')
y  = Int('y')
i  = Int('i')
k  = Int('k')
S = Solver()

#S.add(Not(And([Or(x - y <= 1, x - y >= 3),Or(x - y <= 3, x - y >= 5), x - y >= 1, y == 0])))
#print S.check(),S.model()
A = Function('A',IntSort(),IntSort(),IntSort(),IntSort(),IntSort(),IntSort(),BoolSort())
b1 = [a == 0]
b1.append(b == 0)
b1.append(c == 1)
b1.append(d == 1)
b1.append(e == 0)
b1.append(f == 2)
b1.append(X[0]==1)
b1.append(X[1]==4)

b1.append(A(a,b,c,d,e,f))
b1.append(ForAll([a,b,c,d,i,k],Implies(And([A(a+1,b,c,d,i+1,k),Select(X,i)==1]), A(a,b,c,d,i,k))))
b1.append(ForAll([a,b,c,d,i,k],Implies(And([a == c , b + 1 == d,Select(X,i)==4]), A(a,b,c,d,i,i+1))))
S.add(And(b1))

print S
print S.check(),S.model()


"""[A == 0,
 C >= 0,
 B == 0,
 D >= 0,
 0 + C + D <= 3,
 S(A, B, C, D),
 And(Implies(And(S(a0 + 0, a1 + 1, b0, b1),
                 b0 == c0 - 1,
                 b1 == c1 - 0),
             S(a0, a1, c0, c1)),
     Implies(And(a0 == b0 - 1, a1 == b1 - 0),
             S(a0, a1, b0, b1)),
     b0 == C,
     b1 == D,
     a0 == A,
     b0 == C,
     a1 == B,
     b1 == D)]
"""
