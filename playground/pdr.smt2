(set-option :trasdasdace true)
(declare-rel S (Int Int Int Int Int Int Int Int))
(declare-var a Int)
(declare-var b Int)
(declare-var c Int)
(declare-var d Int)
(declare-var e Int)
(declare-var f Int)


(declare-var h1 Int)
(declare-var h2 Int)

(declare-var i Int)
(declare-var k Int)

(declare-var x Int)
(declare-var y Int)
(declare-var z Int)
(declare-var w Int)
;S -> 0S1
(rule (=> (and (S a (+ b 1) e f (+ i 1) (- k 1) h1 h2) (= (+ e 1) c) (= f d))  (S a b c d i k 0 1)))

;S -> 1
(rule (=> true (S a b (+ a 1) b i i 1 9)))

(declare-rel q1 (Int Int Int Int))
(rule (=> (and (S x y z w i k h1 h2) (= x 0) (= y 0) (= (+ z w) 11)) (q1 x y z w)))
(query q1 :print-certificate true)

