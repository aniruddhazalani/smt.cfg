 #!/usr/bin/env python3
import csv,itertools,getopt
import sys,random,subprocess
sys.path.insert(0,'../z3-debug/build/')
from z3 import *
#set_option(verbose=2)
def wordList(filename):
    a = []

    for i in open(filename, 'r'):
        a.append(i.strip())

    return a

def csvRead(filename):
    a = []

    with open(filename) as csvfile:
        reader = csv.reader(csvfile,delimiter=',')
        for row in reader:
            a.append(row)

    return a

def makeDict(rules):
    dictionary = dict()

    for production in rules:
        if production[0] in dictionary:
            dictionary[production[0]].append(production[1:])
        else:
            dictionary[production[0]] = [production[1:]]

    return dictionary


def numNonterm(prod, nonterminals):
    count = 0

    for token in prod:
        if token in nonterminals:
            count += 1
    return count


def emitStrings(startSym, certiList, tvarList, maxLen):
    output = []
    for i in range(0,len(certiList)):
        certiList[i] = Not(certiList[i])
        #result, model = solveSmt(startSym, certiList, tvarList, maxLen, False)
        outStringEncoding  = solveSmt(startSym, certiList, tvarList, maxLen, False)
        #if str(result) == 'unsat':
         #   continue

#        outStringEncoding = [model[tvar] for tvar in tvarList[1]]
        output.append(outStringEncoding)

        certiList[i] = (Not(certiList[i]))

    return output


def solveSmt(startSym, query, tvarList, maxLen, flag):
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(tvarList[0])):
        queryCond.append(tvarList[0][i] == 0)
        queryCond.append(tvarList[1][i] > 0)

#   length constraint
    queryCond.append(sum(tvarList[1]) <= maxLen)

#    smtSolver = Then('simplify', 'smt').solver()
#    smtSolver = Tactic('qfnra-nlsat').solver()
 #   smtSolver =  Then('simplify', 'nla2bv', 'smt').solver()
    smtSolver = Solver()
    smtSolver.set(mbqi=True)
    
#    smtSolver = Then('simplify', 'elim-term-ite', 'solve-eqs', 'smt').solver()
    smtSolver.assert_and_track(And(queryCond),'model')

    for idx,certificate in enumerate(query):
        if flag:
            if idx == len(query) -1:
                smtSolver.assert_and_track(certificate, 'condition'+str(idx))

            else: 
                smtSolver.assert_and_track(Not(certificate), 'condition'+str(idx))

        else:
            smtSolver.assert_and_track(Not(certificate), 'condition'+str(idx))

    a =  smtSolver.to_smt2()
    """
    for i in tvarList[1]:
        a += '(get-value (' + str(i) + '))\n'
#    a += '(get-model)'
    f = open('a.smt', 'w')
    f.write(a)
    f.close()
    os.system('../z3/build/z3 -smt2  a.smt > out')
    
    val = []
    for i in open('out','r'):
        a = i.split(' ')
        if a[0][:2] == '((' :
            val.append(int(a[1][:-3]))

    return val
"""
    if str(smtSolver.check()) == 'unsat':
        return ('unsat', False)
    
    return (smtSolver.check(), smtSolver.model())
    

def generateLoopQuery(startSym, tvarList, queryStringEncoding):
    
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(tvarList[0])):
        queryCond.append(tvarList[0][i] == 0)

    for i in range(0,len(tvarList[1])):
        queryCond.append(tvarList[1][i] == queryStringEncoding[i])

    return queryCond

def generateQuery(startSym, tvarList, queryString, termList):
    
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(termList)):
        queryCond.append(tvarList[0][i] == 0)
    
    cntTerm = [0 for i in range(0,len(termList))]
    
    for token in queryString:
        if token == '{' or token == '}':
            cntTerm[termList.index(token)] += 1 # TODO paramaterize here

    for i in range(0,len(termList)):
        queryCond.append(tvarList[1][i] == cntTerm[i])

    return queryCond

''' 
    use a for input variables and b for output variables
    for each terminal there are two variables 
    compute index of each terminal
'''

# TODO add debug feature

def main(buggyDict, correctDict,  terminals, nonterminals, queryString):
    
    termList,ntermList = list(terminals),list(nonterminals)
    tempVariable = [chr(ord('a')+i) for i in range(0,20)]
    tvarList = []
    funList,funDict = [],{}
    I,B = IntSort(),BoolSort()

    arg = [I for i in range(0,2*len(termList))]
    arg.append(B)

    for token in nonterminals:
        funList.append(Function(token,arg))
        funDict[token] = funList[-1]

    fp = Fixedpoint()
#   register_relation statement
    for nonTerm in funList:
        fp.register_relation(nonTerm)

#   declare the temporary variables here according to the count of terminals
    for var in tempVariable:
        tvar = []

        for i in range(0,len(terminals)):
            tvar.append(Ints(var+str(i))[0])
        fp.declare_var(tvar)
        tvarList.append(tvar)

# things needed - funList 
# output required - fp 
#   write the function format
    for fun in funList:
        for prod in  buggyDict[fun.name()]:
#            print fun,prod
            cntTerm = [0 for i in range(0,len(terminals))]

            lhs,rhs=[],[]

            if numNonterm(prod,nonterminals) == 0:

                for token in prod:
                    if token == '{' or token == '}':
                        cntTerm[termList.index(token)] += 1 # TODO paramaterize here

                for i in range(0,len(terminals)):
                    rhs.append(tvarList[0][i] == tvarList[1][i] - cntTerm[i])

                lhs.append(tvarList[0])
                lhs.append(tvarList[1])
                lhs = list(itertools.chain(*lhs))

            else:
                curr = 0

                for idx,token in enumerate(prod):
                    if token in nonterminals:
                        t = []

                        for idx,var in enumerate(tvarList[curr]):
                            t.append(var + cntTerm[idx])
                            #print var,cntTerm[idx]

                        k = []
                        k.append(t)
                        k.append(tvarList[curr+1])
                        rhs.append(funDict[token](list(itertools.chain(*k))))
                        curr+=1
                        cntTerm = [0 for i in range(0,len(terminals))]

                    else:

                        if token == '{' or token == '}':
                            cntTerm[termList.index(token)] += 1 # TODO paramaterize here

                        if idx == len(prod) - 1:
                            for i in range(0,len(terminals)):
                                rhs.append(tvarList[curr][i] == tvarList[curr+1][i] - cntTerm[i])

                            curr += 1

                lhs.append(tvarList[0])
                lhs.append(tvarList[curr])
                lhs = list(itertools.chain(*lhs))

#            print lhs,rhs
            fp.rule(fun(lhs),rhs)

    startSym = funDict['S']
    queryCond = generateQuery(startSym, tvarList, queryString, termList)
    result = fp.query(And(queryCond))
#    print fp.get_rules()
#    print fp.to_string(fp.get_rules())
    certificate = fp.get_answer()
    print "outside",certificate
    certiList = [certificate]
    if result == sat:
        print "orignal string satisfiable"

    else:
        maxLen = len(queryString)

        while result == unsat  :
#            result, model = solveSmt(startSym, certiList, tvarList, maxLen, False)

#            queryStringEncoding = [model[tvar] for tvar in tvarList[1]]
          #  print "q",queryStringEncoding

#           generates a new string satisfying the last certificate. This string is assumed to be 
#           in the correct grammar 

            result, model = solveSmt(startSym, certiList, tvarList, maxLen, True)
            if str(result) == 'unsat':
                break

            queryStringEncoding = [model[tvar] for tvar in tvarList[1]]
            print "g",queryStringEncoding

        
#            queryStringEncoding = solveSmt(startSym, certiList, tvarList, maxLen, True)
            query = generateLoopQuery(startSym, tvarList, queryStringEncoding)
            result = fp.query(And(query))

            if result == unsat:
                certiList.append(fp.get_answer())
                print "inside",certiList[-1]
            else:
                print "sat inside loop"
    

        #print certiList, "certificateLen = ", len(certiList)
        return emitStrings(startSym, certiList, tvarList, maxLen)
        #result,model = solveSmt(startSym, certiList, tvarList, len(queryString), False) 
        #print model

# do not assume that the correct and the buggy grammar will have same set of terminals and non terminals 
# implement a function here for each of them. Split up the implementation 
if __name__ == '__main__':
    opts, args = getopt.getopt(sys.argv[1:], "hi:")    
    inputdir = ''

    for opt, arg in opts:
        if opt == '-h':
            print "usage python parseParserDebugFile.py -i <input_directory>"

        elif opt == '-i':
            inputdir = arg

    terminals = set(wordList(inputdir+'/t'))
    nonterminals = set(wordList(inputdir+'/n'))
    buggyRules = csvRead(inputdir+'/r')
    correctRules = csvRead(inputdir+'/correct-grammar')
    queryString = []
#    queryString = ['0','1','1']
#    queryString = ['1','1','1','0','0','0','0']
#    queryString = ['a','a','a','a','a','a','b','b','f','f']
#    queryString = ['a','a','a','a','a','b','b','b','b','f']
    queryStringList = csvRead(inputdir+'/strings')
    buggyDict = makeDict(buggyRules)
    correctDict = makeDict(correctRules)
    for queryString in queryStringList:
        print main(buggyDict, correctDict, terminals, nonterminals, queryString)
