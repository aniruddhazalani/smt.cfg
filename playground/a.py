#!/usr/bin/env python3
import sys
sys.path.insert(0,'../z3/build/')
from z3 import *

I = IntSort()
B = BoolSort()
A = Function('A', I, I, I, I, B)
# A ->  aA | b
fp = Fixedpoint()
fp.register_relation(A)
a0, a1 = Ints('a0 a1')
fp.declare_var(a0, a1)
b0, b1 = Ints('b0 b1')
fp.declare_var(b0, b1)
c0, c1 = Ints('c0 c1')
fp.declare_var(c0, c1)
x0, x1 = Ints('x0 x1')
fp.declare_var(x0, x1)
y0, y1 = Ints('y0 y1')
fp.declare_var(y0, y1)
z0, z1 = Ints('z0 z1')
fp.declare_var(z0, z1)


fp.rule(A(a0, a1,b0, b1),[A(a0 + 1, a1 + 0, b0, b1)])
fp.rule(A(a0, a1,b0, b1),[a0 == b0 + 0, a1 + 1== b1 ])
a =  fp.query(And(A(a0,a1,b0,b1), a0 == 0, b1 == 3, a1 == 0))
b =  fp.get_answer()
s = Solver()
s.assert_and_track(b,'condition')
x = Int('x')
y = Int('y')
s.assert_and_track(A(0,0,x,y), "get model") #give us any model that satisfies A
#print "bbb",b

#print b.to_smt2()
#for i in range(0,b.num_patterns()):
#    print "pat ",b.pattern(i)

#print b.body()

#f = Function('f', I, I, I, I)

#a.assert_and_track( b.body(), 'fucniton')
print(s.check())
kk = s.model()
print(kk)
#print(s.to_smt2())


