
def solveSmt(startSym, correctGrammarEncode, query, tvarList, maxLen, flag):
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])

    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))

    for i in range(0,len(tvarList[0])):
        queryCond.append(tvarList[0][i] == 0)
        queryCond.append(tvarList[1][i] > 0)

#   length constraint
    queryCond.append(sum(tvarList[1]) <= maxLen)

#    smtSolver = Then('simplify', 'smt').solver()
#    smtSolver = Tactic('qfnra-nlsat').solver()
 #   smtSolver =  Then('simplify', 'nla2bv', 'smt').solver()

    smtSolver = Solver()
#    smtSolver.set(unsat_core=True)
    smtSolver.set(mbqi=True)
    smtSolver.set(pull_nested_quantifiers=True)
    
#    smtSolver = Then('simplify', 'elim-term-ite', 'solve-eqs', 'smt').solver()
    smtSolver.assert_and_track(And(correctGrammarEncode),'correctGrammarEncode')
    #print smtSolver
    smtSolver.assert_and_track(And(queryCond),'model')
#    print correctGrammarEncode
    for idx,certificate in enumerate(query):
        if flag:
            if idx == len(query) - 1:
                smtSolver.assert_and_track(certificate, 'condition'+str(idx))

            else: 
                smtSolver.assert_and_track(certificate, 'condition'+str(idx))

        else:
            smtSolver.assert_and_track(certificate, 'condition'+str(idx))

    a =  smtSolver.to_smt2()
    """
    This part has been written to write a smt-lib2 format file and run it if the python api are buggy
    for i in tvarList[1]:
        a += '(get-value (' + str(i) + '))\n'
#    a += '(get-model)'
    f = open('a.smt', 'w')
    f.write(a)
    f.close()
    os.system('../z3/build/z3 -smt2  a.smt > out')
    
    val = []
    for i in open('out','r'):
        a = i.split(' ')
        if a[0][:2] == '((' :
            val.append(int(a[1][:-3]))

    return val
"""
    if str(smtSolver.check()) == 'unsat':
        print "unsat_core"
        print smtSolver.unsat_core()
        return (unsat, False)
    
    return (sat, smtSolver.model())
  
