import sys
sys.path.insert(0,'../z3/build/')
from z3 import *

class AstRefKey:
    def __init__(self, n):
        self.n = n
    def __hash__(self):
        return self.n.hash()
    def __eq__(self, other):
        return self.n.eq(other.n)
    def __repr__(self):
        return str(self.n)

def askey(n):
    assert isinstance(n, AstRef)
    return AstRefKey(n)

def get_vars(f):
    r = set()
    def collect(f):
        if is_const(f):
            if f.decl().kind() == Z3_OP_UNINTERPRETED and not askey(f) in r:
                r.add(askey(f))
        else:
            for c in f.children():
                collect(c)
    collect(f)
    return r


def getVarName(certificate):
    certificate = certificate.arg(0)
    print certificate.children()[0].children()[0].children()
    return [certificate.arg(0).var_name(i)  for i in range(0,certificate.arg(0).num_vars())]

def isOp(expr):
    if is_add(expr) or is_sub(expr) or is_mul(expr) or is_div(expr):
        return True
    return False

def getVarFunc(func):
    numArg = func.num_args()
    return [func.arg(i) for i in range(numArg/2,numArg)]


def getVar(certificate):
    """Write a dfs over the ast to get the name of the variables
        Maintain a set for all the variables. It will have the datatype and the variable
    """
    queue = []
    s = set()
    for i in range(0,certificate.num_args()):
        queue.insert(0,certificate.arg(i))

    while queue != []:
        t = queue.pop()

        if t.sort_kind() != z3.Z3_INT_SORT:
            if t.num_args() != 0:
                for i in range(0,t.num_args()):
                    queue.insert(0,t.arg(i))
        elif isOp(t):
            if t.num_args() != 0:
                for i in range(0,t.num_args()):
                    queue.insert(0,t.arg(i))
        else:
            if not is_const(t):
                s.add(t)

    return s

def dumpSmtquery(nonFunctionCond, var):
    smt2Encode = ''
    for i in var:
        smt2Encode += '(declare-var ?' + str(i)[4:-1] + ' Int)\n'
    s = Solver()
#    s.add(a.children()[0].children()[0] == a.children()[0].children()[0])
    if nonFunctionCond != []:
        s.add(And(nonFunctionCond))
    smt2Encode += s.to_smt2()
    for i in var:
        smt2Encode += '(get-value (?' + str(i)[4:-1] +'))\n'
    smt2Encode += '(get-model)'
    f = open('a.smt', 'w')
    f.write(smt2Encode)
    f.close()
    os.system('../z3-debug/build/z3 -smt2  a.smt > out')

    valDict = dict()
    for i in open('out','r'):
        a = i.split(' ')
        if a[0][:2] == '((' :
            valDict['Var(' + str(a[0][3:]) + ')'] = int(a[1][:-3])

    return valDict


def equateVar(certificateVarList, inp, out):
    conditionList = []
    numVar = len(inp)

    for i in range(0,numVar):
        conditionList.append(certificateVarList[i] == inp[i])
        conditionList.append(certificateVarList[i+numVar] == out[i])
    return conditionList


def extractStringAbstraction(certificate, funList):
    """First extract all the conditions,
        segregate all relations and non relations,
        simplify all the non relations to get values
        identify the closest relation to the start symbol
        Track it back to get the string abstraction
    """
    if str(certificate.decl()) == 'And':
        conditionList = [certificate.arg(i)  for i in range(0,certificate.num_args())]
    else:
        conditionList = [certificate]
    nonFunctionCond = []
    funSet = set(funList)

    startSym = 0

    for i in funList:
        if str(i) == 'S':
            startSym = i
    reqVar = []

    flag  = 1
    for condition in conditionList:
        if condition.decl() not in funSet:
            nonFunctionCond.append(condition)
        else:
            if flag == 1:
            #if condition.decl() == startSym:
                reqVar = getVarFunc(condition)
                flag = 0

    valDict = dumpSmtquery(nonFunctionCond,getVar(certificate))
    
    abstraction = [0 for i in range(0,len(reqVar))]

    for condition in conditionList:
        if condition.decl() in funSet:
            varList = getVarFunc(condition)

            for idx,var in enumerate(varList):
                if str(var) in valDict:
                    abstraction[idx] = max(abstraction[idx],valDict[str(var)])
#                    abstraction.append(valDict[str(i)])
                else:
                    if abstraction[idx] < int(str(var)):
                        abstraction[idx] = var
                    #abstraction.append(i)

    return abstraction


#def getVar(ans):
    """returns var in a certificate in form of a list """
#    a =  ans.arg(0)
 #   return a.body().children()[0].children()[0]
#    return [Int(ans.arg(0).var_name(i)) for i in range(0,ans.arg(0).num_vars())]

    """
    return [Int(ans.arg(0).var_name(i)) for i in range(0,ans.arg(0).num_vars())]
    #print [ans.arg(0).children()[0].children()[0] for i in range(0,ans.arg(0).num_vars())]
    return [ans.arg(0).children()[0].children()[0] for i in range(0,ans.arg(0).num_vars())]
    return [ans.arg(0).children()[0].children()[0].arg(i) for i in range(0,ans.arg(0).num_vars())]
    print "certificate",ans.arg(0).children()[0].children()[0].decl()
    print "---------------------------------------"
    lastAnd = ans.arg(0).children()[-1]
    print "lastAnd",lastAnd
    print "---------------------------------------"
    print len(lastAnd.children())
    print "---------------------------------------"
    trace = lastAnd.children()[1]
    print "trace",trace
    print "---------------------------------------"
    while trace.num_args() > 0:
        print trace.num_args()
        print trace.decl()
        trace = trace.children()[-1]
        print "aaa",trace
        print "---------------------------------------????/"
    """


    """


t = Then('simplify',
        'solve-eqs')

#    x = Int('?26')
a = nonFunctionCond[0]
b = nonFunctionCond[1]
#a = [Not(x > 0), x < 2]
s = Goal()
i = Ints('i')
s.add(And(nonFunctionCond))
r = t(s)
dumpSmtquery(nonFunctionCond,getVar(certificate))
print "asd",r
s = Solver()
#    s.add(a.children()[0].children()[0] == a.children()[0].children()[0])
s.add(And(nonFunctionCond))
print s.to_smt2()

#    print s.assertions()
print "condition asserted"
#print s.check()
exit(-1)


    """
