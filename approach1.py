#!/usr/bin/env python3
import csv,itertools,argparse
import sys
sys.path.insert(0,'../z3/build/')
from z3 import *

def wordList(filename):
    a = []
    for i in open(filename, 'r'):
        a.append(i.strip())
    return a

def csvRead(filename):
    a = []
    with open(filename) as csvfile:
        reader = csv.reader(csvfile,delimiter=',')
        for row in reader:
            a.append(row)
    return a

def makeDict(rules):
    dictionary = dict()
    for production in rules:
        if production[0] in dictionary:
            dictionary[production[0]].append(production[1:])
        else:
            dictionary[production[0]] = [production[1:]]
    return dictionary

def numNonterm(prod, nonterminals):
    count = 0
    for token in prod:
        if token in nonterminals:
            count += 1
    return count

def solveSmt(startSym, query, tvarList):
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])
    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))
    for i in range(0,len(tvarList[0])):
        queryCond.append(tvarList[0][i] == 0)
        queryCond.append(tvarList[1][i] >= 0)
#    queryCond.append(tvarList[1][0] + tvarList[1][1]  <= 1  )
    smtSolver = Solver()
    smtSolver.assert_and_track(And(queryCond),'model')
    smtSolver.assert_and_track((query), 'condition')
 #   print "assertions",smtSolver.assertions()
    #print smtSolver.to_smt2()
#    smtSolver.assert_and_track(k[2] + k[3] <= 4, "length condition")
    return (smtSolver.check(), smtSolver.model())
 #   print a[k[2]],a[k[3]]
    
#    t =  [funDict['A'](list(itertools.chain(*k))), tvarList[0][0] == 0, tvarList[0][1] == 0, tvarList[1][1] == 0]
def generateLoopQuery(startSym, tvarList, queryStringEncoding):
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])
    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))
    for i in range(0,len(tvarList[1])):
        queryCond.append(tvarList[1][i] == queryStringEncoding[i])
    return queryCond

def generateQuery(startSym, tvarList, queryString, termList):
    k,queryCond = [],[]
    k.append(tvarList[0])
    k.append(tvarList[1])
    k = list(itertools.chain(*k))
    queryCond.append(startSym(k))
    for i in range(0,len(termList)):
        queryCond.append(tvarList[0][i] == 0)
    cntTerm = [0 for i in range(0,len(termList))]
    for token in queryString:
        cntTerm[termList.index(token)] += 1 # TODO paramaterize here

    for i in range(0,len(termList)):
        queryCond.append(tvarList[1][i] == cntTerm[i])

    return queryCond

''' 
    use a for input variables and b for output variables
    for each terminal there are two variables 
    compute index of each terminal
'''

# TODO add debug feature

def writeFile(rules, terminals, nonterminals, queryString):
    termList,ntermList = list(terminals),list(nonterminals)
    tempVariable,tvarList = ['a','b','c','x','y','z'],[]
    funList,funDict = [],{}
    I,B = IntSort(),BoolSort()

    arg = [I for i in range(0,2*len(termList))]
    arg.append(B)

    for token in nonterminals:
        funList.append(Function(token,arg))
        funDict[token] = funList[-1]

    fp = Fixedpoint()

#   register_relation statement
    for nonTerm in funList:
        fp.register_relation(nonTerm)

#   declare the temporary variables here according to the count of terminals
    for var in tempVariable:
        tvar = []
        for i in range(0,len(terminals)):
            tvar.append(Ints(var+str(i))[0])
        fp.declare_var(tvar)
        tvarList.append(tvar)

#   write the function format
    for fun in funList:
        for prod in  rules[fun.name()]:
            cntTerm = [0 for i in range(0,len(terminals))]
#            print  fun,prod
            lhs,rhs=[],[]
            if numNonterm(prod,nonterminals) == 0:
                for token in prod:
                    cntTerm[termList.index(token)] += 1 # TODO paramaterize here
                for i in range(0,len(terminals)):
                    rhs.append(tvarList[0][i] == tvarList[1][i] - cntTerm[i])
                lhs.append(tvarList[0])
                lhs.append(tvarList[1])
                lhs = list(itertools.chain(*lhs))
            else:
                curr = 0
                for idx,token in enumerate(prod):
                    if token in nonterminals:
                        t = []
                        for idx,var in enumerate(tvarList[curr]):
                            t.append(var + cntTerm[idx])
                            #print var,cntTerm[idx]
                        k = []
                        k.append(t)
                        k.append(tvarList[curr+1])
                        rhs.append(funDict[token](list(itertools.chain(*k))))
                        curr+=1
                    else:
                        cntTerm[termList.index(token)] += 1 # TODO paramaterize here
                        if idx == len(prod) - 1:
                            for i in range(0,len(terminals)):
                                rhs.append(tvarList[curr][i] == tvarList[curr+1][i] - cntTerm[i])
                            curr += 1

                lhs.append(tvarList[0])
                lhs.append(tvarList[curr])
                lhs = list(itertools.chain(*lhs))

#            print lhs,rhs
            fp.rule(fun(lhs),rhs)
    startSym = funDict['A']
    queryCond = generateQuery(startSym, tvarList, queryString, termList)
    result = fp.query(And(queryCond))
    query = fp.get_answer()
    interpolant = [query]
    if str(result) == 'sat':
        print "orignal string satisfiable"
    while str(result) == 'unsat':
        result, model = solveSmt(startSym, query, tvarList)
        print "model",model
        queryStringEncoding = [model[tvar] for tvar in tvarList[1]]
        queryCond = generateLoopQuery(startSym, tvarList, queryStringEncoding)  
        result = fp.query(And(queryCond))
        query = fp.get_answer()
        if str(result) == 'unsat':
            interpolant.append(query)

    print interpolant


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='provide file names')
    terminals = set(wordList('./tests/t3'))
#    print terminals
    nonterminals = set(wordList('./tests/n3'))
    rules = csvRead('./tests/r3')
    queryString = ['a','a','a','b','b']
    dictionary = makeDict(rules)
#    print(dictionary['xor_expr'])
    writeFile(dictionary, terminals, nonterminals, queryString)

