\section{Overview}
In this subsection, we will explain the preliminaries and also provide a high level overview of our algorithm with the help of an example.
 \numberwithin{equation}{subsubsection}

\subsection{Preliminaries}
\subsubsection{Context-free Grammars}
A context free grammar is a tuple which consists of $(N, \Sigma, P, S)$, where 
\begin{itemize}
\item $S$ denotes the starting non-terminal.
\item $N$ denotes finite a set of non-terminal symbols.
\item $\Sigma$ denotes a finite set of terminal symbols 
\item $P$ denotes a finite set of production rules. Each production $p\in P$ is a finite 
relation from $N$ to $(N \cup \Sigma)^*$.
\end{itemize}. 

\subsubsection{Horn Clause}
Horn clause \cite{JSL:9106942} \cite{horn} is a clause with at most one positive literal. 
Three type of propositional statements can be identified as a horn clause:
\begin{itemize}
\item \textbf{Rules}: It must contain one positive literal and at least one negative literal. A rule has the form $\neg B_1 \lor \neg B_2 \lor \ldots \neg B_n \land A$, where $B_1$, $B_2 \ldots B_n$ and $A$ are positive literals. It is logically equivalent to the form  $ B_1 \land B_2 \land \ldots  B_n \implies A$. $A$ is called the \textit{head} and $ B_1 \land B_2 \land \ldots  B_n$ is called \textit{body} of the horn clause.

For example, rule for "all men are mortal" will be $\neg$ man(X) $\lor$ mortal(X). The 
rule is universally quantified over X. Hence, X is a free variable.
\item \textbf{Fact}: It must contain one positive literal and zero negative literal. For example, $A$.
\item \textbf{Integrity Constraint}: It must contain zero positive literal and at least 
one negative literal. It is of the form $\neg B_1 \lor \neg B_2 \lor \ldots \neg B_n$. 
The integrity constraint is the negation of the statement to be proven or $goal$. A 
$goal$ is a single literal or conjunction of literals. 

For example, if we want to show that "there exists a man who is mortal". It can be 
represented as $\exists $ X (man(X) $\land$ mortal(X)). It's integrity constraint will be 
$\forall$ X ($\neg$ man(X) $\lor \neg$ mortal(X)). In the integrity constraint X is a 
free variable.
\end{itemize}

However in this thesis, we use the nomenclature used by common horn clause solvers, where both facts and rules are refered to as rules; integrity constrains are refered to as queries.

\subsubsection{Horn Clause Solver}
Given horn clause rules  and integrity constraint, horn clause solvers can automatically 
check if the integrity constraint can be satisfied. Some of the approaches to 
solve horn clauses are the duality algorithm \cite{duality} and the property directed reachability 
algorithm \cite{hoder2012generalized}, which are implemented inside the Z3 solver \cite{de2008z3}.

When we check satisfiability of an integrity constraint on horn clauses, the 
solver returns satisfiable ($sat$) or unsatisfiable ($unsat$). In case the solver returns $sat$, it also outputs a witness of satisfiability and in the other case it returns a \textbf{certificate}.
A certificate is an over-approximation of all the reachable states from an 
initial state. It contains boolean clauses in the conjunctive normal form which indicates the reasons for $sat$ or $unsat$.

Consider example in Figure \ref{fig:horn-clause-factorial}. It shows the encoding of the factorial program as horn clause. Input and output state variables to the factorial function are $n$ and $m$ respectively. \\

\begin{program}
\caption{Factorial program as a horn clause}
\label{fig:horn-clause-factorial}
\begin{minted}{scheme}
(declare-rel factorial (Int Int))

;Rule 1: factorial(0) = 1
(rule (=> (= n 0) (factorial n 1))) 

;Rule 2: factorial(1) = 1
(rule (=> (= n 1) (factorial n 1)))

;Rule 3: factorial(n) = factorial(n-1)*n
(rule (=> (and (> n 1) (factorial (- n 1) p) (= m (* p n))) (factorial n m)))

(declare-rel q1 (Int Int))
(rule (=> (and (factorial n m) (< n 2) (= m 2)) (q1 n m))) 
(query q1 :print-certificate true)

(declare-rel q2 (Int Int))
(rule (=> (and (factorial n m) (>= n 2) (= m 2)) (q1 m n))) 
(query q1 :print-certificate true)
\end{minted}
\end{program}

Two queries have been posed to the horn clause solver. 
\begin{itemize}
\item  Query q1 checks if there exists $n$ and $m$  such that $n$ is less than 2 and $m$ is equal to 2. Clearly, for all $n$ less than 2 factorial will return 1. The result of the query is $unsat$. and following certificate is produced is $(\neg (n \leq 1)) \lor ( m \leq 1))$. It states that for values of input state variable less than or equal to 1, the output state variable can take values which are less than or equal to 1.

\item Query q2, checks of there exists $n$ and $m$ such that $n$ is greater than or equal to 2 and $m$ is equal to 2. The result of the query is $sat$ and the certificate contains a witness for satisfiability. The  certificate which was obtained in this case is  (factorial 2 2) $\land$ (factorial 1 1). The certificate indicates that factorial(2) = 2 because the factorial(1) = 1.
\end{itemize}


\subsubsection{Abstract Interpretation}
Let $\mathscr{C}$ be an partially ordered set called a concrete set and let $\mathscr{A}$ be another partially ordered set, called an abstract set. ($\alpha$,$\gamma$) is a Galois connection \cite{cousot1977abstract} between $\mathscr{C}$ and $\mathscr{A}$, where $\alpha$: $\mathscr{C} \rightarrow \mathscr{A}$ and $\gamma$: $\mathscr{A} \rightarrow \mathscr{C}$ ,such that the following properties hold:
\begin{itemize}
\item $\alpha$ and $\gamma$ are order preserving.
\item $a \in \mathscr{A}, a = \alpha(\gamma(x))$.
\item $c \in \mathscr{C}, c	 \leq \alpha(\gamma(c))$.
\end{itemize}
\noindent
$\alpha$ is called the \textbf{abstraction function} and $\gamma$ is called the \textbf{concretization function}. $\alpha(c)$ is called the abstraction of $c$ and $\gamma(a)$ is called the concretization of $a$.
\subsection{Notations}
We use upper case letters denote the non-terminals and lower case letters denote terminals. Greek letters $\beta, \delta$  to represent strings consisting of terminals and non-terminals. Language of a context-free grammar is denoted by $\mathscr{L}$(G).  We use $[|G|]$ to denote encoding of a grammar. 
The notation $FP(rules, query)$  denotes an execution of a fixed-point solver, which takes horn clause rules, $rules$ and a set of integrity constraints, $query$, as input and outputs a result $r$ and a certificate $\sigma$. $r$ could be $sat$ or $unsat$.
%fixe fp

\subsection{Problem Statement}\label{sec:prob-stmt}
%explain algorithm with detailed example 
\textbf{Goal}: \textit{Given a buggy context-free grammar $G_{buggy}$ and a correct grammar $G_{oracle}$, generate tests for two types of errors.
\begin{itemize}
\item[1] \textbf{Type-A} errors: strings $w \in \mathscr{L}(G_{oracle})$ but $w \not\in L(G_{buggy})$.
\item[2] \textbf{Type-B} errors: strings $w \in \mathscr{L}(G_{buggy})$ but $w \not\in \mathscr{L}(G_{oracle})$.
\end{itemize}
Ideally, each test string should represent a different bug in $G_{buggy}$.}

With respect to \textit{Type-A} errors, it can be possible that multiple strings can not be parsed by the $G_{buggy}$ due to a specific bug. We intend to provide only one test string to indicate that bug.

For example for the grammars in Figure \ref{fig:demo}, test strings depicting Type-A errors are ``", ``a",``b", ``c", ``aa", ''bbb", ''bbbb". Note that the test string ``" and ``aa" point to the same bug in the student's submission (starting non-terminal S can not produce $\epsilon$). Our tool will only give one test case to indicate this bug.

For Type-B errors; as  $\mathscr{L}(G_{oracle})$ admits strings which are palindromes, strings ``abc", ``aabca", ``abcaabc" are invalid. All of these test strings are produced by the production rule S $\rightarrow$ \texttt{a} \texttt{b} \texttt{c}. If we fix this rule, then the three test strings will not be produced by $G_{buggy}$. So, they represent only one bug in $G_{buggy}$. Our tool outputs ``abc", which is representative of the error in $G_{buggy}$. 

There are two more bugs in the grammar which were discussed in Section \ref{chap:intro}.

\subsection{Overview of the algorithm}
In this work, we observe that context-free grammars can be encoded elegantly in the form of horn clauses. As Horn clause solvers can operate only on numerical constraints, we design abstraction functions that encode strings as numerical relations.
%copy
\subsubsection{Abstraction Functions}
Let the concrete set $\mathscr{C}$ be the set of strings, that is $\mathscr{C}$ = $\Sigma^*$ and abstract set $\mathscr{A}$ be tuple of integers. We design abstraction functions on a set of strings as a vector of functions where each function $f_{t_i}: \Sigma \times \mathbb{Z}  \rightarrow 
\mathbb{Z}$, that is it maps each terminal and its input state variable to an output state variable. Every function $f_{t_i}$ corresponds to a terminal $t_i$. Formally, $\alpha = <f_{t_1}, f_{t_2} \dots f_{t_n}>$.
Given a string $w = [c_1, c_2 \ldots c_k]$, if $t_i = c_j$, then $f_{t_i}$ takes $c_j$ and the input state variable for $t_i$ and outputs $f_{t_i}(c_j,in)$.
 In our current implementation we include the following abstraction functions:
\begin{itemize}
\item[1.] \textbf{Counting abstraction}: It counts the number of occurances of a terminal in the string. 
\begin{equation}
 f_{t_i}(c,in) =
  \begin{cases} 
      \hfill in + 1   \hfill & \text{ if index of $c$ is equal to $t_i$ } \\
      \hfill in   \hfill & \text{otherwise} \\
  \end{cases} \label{eq:count-abs}
\end{equation}
 where $in$ is an integer denoting the input state variable of the abstraction and $c$ is the terminal.

\item[2.] \textbf{Inclusion-exclusion abstraction function}: It captures the presence of a terminal in a string.

\begin{equation}
  f_{t_i}(c,in) = \begin{cases}
   \hfill  in \lor 1 \hfill & \text{if index of $c$ is equal to $t_i$}\\
   \hfill  in \lor 0 \hfill  & \text{otherwise}
  \end{cases} \label{eq:incl-excl}
\end{equation}
 where $in$ is an integer denoting input state variable of the abstraction and $c$ is the terminal.

\end{itemize}
%We use $\alpha(s): \Sigma^* \rightarrow \mathbb{Z}^*$ to denote an abstraction of a string $s$.

\subsubsection{Algorithm via an Example}
Given a buggy context-free grammar $G_{buggy}$ and a correct grammar 
$G_{oracle}$ , we need to find strings which are representative of the bugs 
in $G_{buggy}$.  Intially we obtain a property $\phi_1$ from $G_{buggy}$ with 
the help of a string $s_1$. $\phi_1$ contains the reasons of parsing failure 
of $s_1$. Therefore, $\phi_1$ represents a fault in $G_{buggy}$. When we try to 
satisfy $\phi_1$ with $G_{oracle}$, we get another string $s_2$, which  
represents a different bug. With the help of $s_2$, we obtain another 
property $\phi_2$ from $G_{buggy}$. Note that $\phi_1$ and $\phi_2$ can not be 
same because $s_2$ does not have same bugs represented by $\phi_1$. In the 
next step, we negate $\phi_1$ and $\phi_2$ and satisfy it with $G_{oracle}$.  We will get another string $s_3$. We repeat this process till we fail to get 
a string which fails on $G_{buggy}$. 

All the properties $\phi_1, \phi_2 \ldots \phi_k$ represent bugs in $G_{oracle}$. Using these properties we extract strings from $G_{orcale}$. These strings are representative of a different bug in $G_{buggy}$.
\begin{figure}[t]
\includegraphics[width=18cm]{algorithm.pdf}
\caption{Three phases of our algorithm}
\label{fig:three-phases}
\end{figure}


%change A
Consider Figure \ref{fig:demo}: $\mathcal{A}$ and $\mathcal{D}$ are input and output variables for terminal \texttt{a}, $\mathcal{B}$ and $\mathcal{E}$ are input and output variables for terminal \texttt{b} and $\mathcal{C}$ and $\mathcal{F}$ are input and output variables for terminal \texttt{c}. 

\begin{program}
\caption{Palindrome grammar}
\label{fig:demo}
\begin{minted}{scheme}
Correct grammar          Student's submission

S -> a S a               S -> a S a 
  -> b S b                 -> S a S 
  -> c S c                 -> a b c 
  -> a 
  -> b 
  -> c 
  -> 
\end{minted}
\end{program}
%write in abtract terms about the algorithm
We demonstrate our algorithm to find $Type A$ errors. 
\begin{enumerate}

	\item\textbf{ Certificate Generation:}
	\begin{enumerate}
	
\item To \textbf{initialize the algorithm} we require an abstraction of a string that cannot be produced by $G_{buggy}$. For counting abstraction function \eqref{eq:count-abs} if the input variables ($\mathcal{A}$,$\mathcal{B}$,$\mathcal{C}$) = (0,0,0), then the values of the variables can not be negative as we always add one on the occurance of a terminal. Therefore, we use $\alpha(s_{initial})$ =  (-1,-1,-1) as our initial abstraction of the string. We pose $\alpha(s_{initial})$ as a $goal$ to the horn clause solver.
\item As the $\alpha(s_{initial})$ can not be computed by $G_{buggy}$, the horn clause solver returns $unsat$ and a certificate. The certificate contains the model of $G_{buggy}$. The certificate obtained for our exmaple $S(\mathcal{A}$,$\mathcal{B}$,$\mathcal{C}, \mathcal{D}$,$\mathcal{E}$,$\mathcal{F}) == (\mathcal{D}  - \mathcal{A} \geq 1))$. It indicates that if the non-terminal S is applied then every string will have number of \texttt{a} increases by one.
% fix here
 Note that this clearly not the case for $G_{oracle}$. So this certificate represents an error.
\item Next, we query the certificate obtained in Step (b) on $G_{oracle}$'s horn clause solver and obtain an abstraction of a string $\alpha(s_1)$.
\item We query $\alpha(s_1)$ on $G_{buggy}$'s horn clause solver as in Step (a). The certificate obtained in this step is $S(\mathcal{A}$,$\mathcal{B}$,$\mathcal{C}, \mathcal{D}$,$\mathcal{E}$,$\mathcal{F}) == (\mathcal{E} - \mathcal{B} \geq 1))$. It indicates that if non-terminal S is applied then every string will have number of \texttt{b} is greater than one. This is also not true as string \texttt{c} is in the language of $G_{oracle}$. As evident the certificate obtained in this step  reflects upon more properties of $G_{buggy}$.
\item We repeat steps (b), (c), (d) till we obtain an abstraction of a string which is satisfiable on $G_{buggy}$'s horn clause solver. 

	
	\end{enumerate}
	
	\item \textbf{ Abstraction Computation phase:}
	\begin{enumerate}
	\item The output is a list of certificates from which we find abstraction of the strings which are directed towards specific bugs.
	\end{enumerate}
	\item \textbf{String Generation Phase:}
	\begin{enumerate}
	
		\item  We obtain strings from the abstractions of the strings. We accomplish it via horn clause solver.
% put sub numbers
		\item The strings obtained for grammars in Figure \ref{fig:demo}  are "", "b" and "c". The tests are in the language of $G_{oracle}$ but not in the language of $G_{buggy}$. Each test indicates a different bug in the student grammar. The empty string ``", indicates that S $\rightarrow$ $\epsilon$ should be added. Similarly, strings "b" and "c" indicate that the production rules S $\rightarrow$ b and S $\rightarrow$ c should also be added to the student's grammar.
	
	\end{enumerate}
	

\end{enumerate}

\subsubsection{Handling Type-B errors}
\textit{Type-B} errors can be computed via swapping $G_{buggy}$ and $G_{oracle}$ in our algorithm.
%
%\subsubsection{Intuition}
%Given two grammars $G_{buggy}$ and $G_{oracle}$, we first compute the encode grammar as horn clauses. First we check 
%%flowchart 

%Each production in the grammar is encoded as mentioned in \ref{comp_prod_encod} with the input and output variables all terminals in $\Sigma$, start and end index of the string and the string produced. Each terminal is identified by a unique integer. Therefore, the representation of the string is an integer. For the example 
%Next state variables. Take a specific combination of values of the next state variables we have one point. If there exists an input combination such that the property fails, we say that the set of bits for the next use variables is bad. 

%Compute set of cubes which fully contain bad states, its complement will exclude bad state. It will be a property directed over-approximation of the reachable states. 


